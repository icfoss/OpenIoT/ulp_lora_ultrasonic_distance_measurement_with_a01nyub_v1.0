## ULP_LoRa_ultrasonic_distance_measurement_with_a01nyub_v1.0
---
![image-1.png](https://gitlab.com/abdularshadvs/ulp_lora_ultrasonic_distance_measurement_with_a01nyub_v1.0/-/raw/main/Images/image-1.png?ref_type=heads)

### _Overview_
---
The ULP_LoRa ultrasonic distance measurement with a01nyub v1.0 project builds a system for remote distance measurement using ultrasonic technology and transmits the data using LoRaWAN technology. The system can be used for various applications that deal with measuring the distance between two points (up to 750 cm), This is achieved by using an A01NYUB Waterproof Ultrasonic Sensor to measure the distance between the sensor and the target object. Ultrasonic sensors can achieve highly accurate measurements, often within millimetres or centimetres due to their high-frequency sound waves while maintaining the low cost and simple construction feature and the LoRaWAN technology enables long range low power data communication for remote monitoring making it ideal for IoT devices. The micro-controller board which is used here is called ULP LoRa which an Arduino pro-mini based microcontroller, and the system is designed in such a way that the controller powers up the sensor, collects the data, transmit it and goes back to sleep while also powering off the sensor to further save the power.  

### _Features_
---
- LoRaWAN compatible
- Distance measurement accuracy ±1cm 
- Detection range 28-750 cm

### _Recorderd Parameters_
---
- Distance in mm
- Battery voltage

### _Pre-Requisite_
---
- [Arduino IDE 1.8.19](https://www.arduino.cc/en/software)
- [MCCI LoRaWAN LMIC library](https://gitlab.com/abdularshadvs/ulp_lora_ultrasonic_distance_measurement_with_a01nyub_v1.0/-/tree/main/Software/libraries/MCCI_LoRaWAN_LMIC_library?ref_type=heads)
- [A01NYUB Waterproof Ultrasonic Sensor](https://www.dfrobot.com/product-1934.html)
- [Battery](https://robu.in/product/panasonic-ncr-18650b-3400-mah-li-ion-battery/)
- [ULP_LoRa board](https://gitlab.com/icfoss/OpenIoT/ulplora.git)
- [FTDI](https://robu.in/product/ttl-ftdi-transmitter-usb-3-3v-5v-dual-power-ft232rl-ftdi-mwc-programmer/)

### _Getting Started_
---
- connect the components as shown in the wiring diagram [here](https://gitlab.com/abdularshadvs/ulp_lora_ultrasonic_distance_measurement_with_a01nyub_v1.0/-/blob/main/Hardware/Wiring_diagrams/a01nyub_ULP_LoRA%20wiring%20diagram.pdf?ref_type=heads)
- clone the repository
    ``` sh
    git clone https://gitlab.com/abdularshadvs/ulp_lora_ultrasonic_distance_measurement_with_a01nyub_v1.0.git
    ```
- open Arduino IDE and import the ***a01nyub_with_lmic*** folder from the cloned repository
- also install the MCCI LoRaWAN lmic library provided inside the ***libraries*** folder
- configure the keys and address with the keys and address obtained from the chirpstack / Things network
    ```c
    // Set Transmission interval in seconds
    const unsigned long TX_INTERVAL = 180; 
    
    // Set Network session key in Hex Array Format
    static const PROGMEM u1_t NWKSKEY[16] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

    // Set App session key in Hex Array Format
    static const u1_t PROGMEM APPSKEY[16] = { 0x00, 0x0,0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

    // Set Device address
    static const u4_t DEVADDR = 0x00000000 ;    
    ```
    <br>

    ```c
    // If using external radio module do change the pin configuration accordingly

    const lmic_pinmap lmic_pins = {
    .nss = 6,                       // chip select on 
    feather (rf95module) CS
    .rxtx = LMIC_UNUSED_PIN,
    .rst = 5,                       // reset pin
    .dio = {2, 3, LMIC_UNUSED_PIN},
    };
    ```
    
- connect the ULP LoRa board to the FTDI as shown in the programmer connection diagram [here](https://gitlab.com/abdularshadvs/ulp_lora_ultrasonic_distance_measurement_with_a01nyub_v1.0/-/blob/main/Hardware/Programmer_connection_diagram/Programmer_connection_diagram.pdf?ref_type=heads)
- select the arduino pro-mini form the board selector
- upload the program

### _Acknowledgements_
---
- This project is build upon the base code of [MCCI LoRaWAN lmic package](https://github.com/mcci-catena/arduino-lorawan) 

### _License_
---
This project is licensed under the MIT License - see the LICENSE.md file for details
