/**
  ******************************************************************************
  * @file  a01nyub_with_limic.ino
  * @author CDOH Team
  * @brief  Driver for a01nyub
  *         This file provides firmware functions to manage the following
  *         functionalities
  *           + Initializes necessary pins for the sensors
  *           + Read data from the sensors
  *           + Transmits the data via LoRaWAN network at a fixed interval
  *           + Power down the sensors and goes into sleep mode
 **/


/*******************************************************************************

   This example sends a valid LoRaWAN packet with payload a01nyub sensor data along with battery voltage, using frequency and encryption settings matching those of
   the The Things Network.

   This uses ABP (Activation-by-personalisation), where a DevAddr and
   Session keys are preconfigured (unlike OTAA, where a DevEUI and
   application key is configured, while the DevAddr and session keys are
   assigned/generated in the over-the-air-activation procedure).

   Note: LoRaWAN per sub-band duty-cycle limitation is enforced (1% in
   g1, 0.1% in g2), but not the TTN fair usage policy (which is probably
   violated by this sketch when left running for longer)!

   To use this sketch, first register your application and device with
   the things network or chirpsatck, to set or generate a DevAddr, NwkSKey and
   AppSKey. Each device should have their own unique values for these
   fields.

   Do not forget to define the radio type correctly in
   arduino-lmic/project_config/lmic_project_config.h or from your BOARDS.txt.

 *******************************************************************************/


/**
 ********************************************* 
 * *****Include the required libraries *******
 *********************************************
 **/
 
#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>
#include <SoftwareSerial.h>
#include "LowPower.h"

/**
 * @defgroup 
 **/
 
#define SLEEP
#define read_duration        30000
#define sensor_power_pin     10
#define sensor_tx_pin        7
#define sensor_rx_pin        8
#define CFG_in866 1
#define battery_pin          A3
#define Sample_rate          10

bool next = false;

unsigned char data[4] = {0};
uint16_t distance;
unsigned long int previous_time = 0;
unsigned long int current_time = 0;

/**
 * Schedule TX every this many seconds and set the payload
 **/
 
const unsigned TX_INTERVAL = 60;
static uint8_t mydata[4] = {0};
SoftwareSerial myserial(sensor_rx_pin, sensor_tx_pin);

/**
 * LoRaWAN NwkSKey, network session key
 * This should be in big-endian (aka msb).
 **/
 
static const PROGMEM u1_t NWKSKEY[16] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

/**
 * LoRaWAN AppSKey, application session key
 * This should be in big-endian (aka msb).
 **/

static const u1_t PROGMEM APPSKEY[16] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

/**
 * LoRaWAN end-device address (DevAddr)
 * See http://thethingsnetwork.org/wiki/AddressSpace
 * The library converts the address to network byte order as needed, so this should be in big-endian (aka msb) too.
 **/
 
static const u4_t DEVADDR = 0x00000000 ;

/**
 * These callbacks are only used in over-the-air activation, so they are left empty here (we cannot leave them out completely unless
 * DISABLE_JOIN is set in arduino-lmic/project_config/lmic_project_config.h, otherwise the linker will complain).
 **/
 
void os_getArtEui (u1_t* buf) { }
void os_getDevEui (u1_t* buf) { }
void os_getDevKey (u1_t* buf) { }

static osjob_t sendjob;

/**
 * Pin mapping
 **/
 
const lmic_pinmap lmic_pins = {
  .nss = 6,
  .rxtx = LMIC_UNUSED_PIN,
  .rst = 5,
  .dio = {2, 3, LMIC_UNUSED_PIN},
};

void onEvent (ev_t ev) {
  Serial.print(os_getTime());
  Serial.print(": ");
  switch (ev) {
    case EV_SCAN_TIMEOUT:
      Serial.println(F("EV_SCAN_TIMEOUT"));
      break;
    case EV_JOINING:
      Serial.println(F("EV_JOINING"));
      break;
    case EV_JOINED:
      Serial.println(F("EV_JOINED"));
      break;
    case EV_JOIN_FAILED:
      Serial.println(F("EV_JOIN_FAILED"));
      break;
    case EV_REJOIN_FAILED:
      Serial.println(F("EV_REJOIN_FAILED"));
      break;
    case EV_TXCOMPLETE:
      Serial.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
      if (LMIC.txrxFlags & TXRX_ACK)
        Serial.println(F("Received ack"));
      if (LMIC.dataLen) {
        Serial.println(F("Received "));
        Serial.println(LMIC.dataLen);
        Serial.println(F(" bytes of payload"));
      }
      
      /****schedule next transmission****/
      
#ifndef SLEEP
      os_setTimedCallback(&sendjob, os_getTime() + sec2osticks(TX_INTERVAL), do_send);
#else
      next = true;
#endif
      break;
    case EV_RESET:
      Serial.println(F("EV_RESET"));
      break;
    case EV_RXCOMPLETE:
      // data received in ping slot
      Serial.println(F("EV_RXCOMPLETE"));
      break;
    case EV_TXSTART:
      Serial.println(F("EV_TXSTART"));
      break;
    case EV_TXCANCELED:
      Serial.println(F("EV_TXCANCELED"));
      break;
    case EV_RXSTART:
      /* do not print anything -- it wrecks timing */
      break;
    case EV_JOIN_TXCOMPLETE:
      Serial.println(F("EV_JOIN_TXCOMPLETE: no JoinAccept"));
      break;
    default:
      Serial.print(F("Unknown event: "));
      Serial.println((unsigned) ev);
      break;
  }
}

void do_send(osjob_t* j) {
  // Check if there is not a current TX/RX job running
  if (LMIC.opmode & OP_TXRXPEND) {
    Serial.println(F("OP_TXRXPEND, not sending"));
  } else {

    read_distance();

    batteryVoltage();

    LMIC_setTxData2(2, mydata, sizeof(mydata), 0);
    Serial.println(F("Packet queued"));
  }
}

void setup() {

  Serial.begin(115200);
  delay(100);
  pinMode(sensor_power_pin, OUTPUT);
  digitalWrite(sensor_power_pin, LOW);
  delay(1000);

  Serial.println(F("Starting"));

  // LMIC init  
  os_init();
  // Reset the MAC state. Session and pending data transfers will be discarded.
  LMIC_reset();
  // Use with Arduino Pro Mini ATmega328P 3.3V 8 MHz
  // Let LMIC compensate for +/- 1% clock error
  LMIC_setClockError(MAX_CLOCK_ERROR * 1 / 100);
  // Set static session parameters. Instead of dynamically establishing a session
  // by joining the network, precomputed session parameters are be provided.
#ifdef PROGMEM
  // On AVR, these values are stored in flash and only copied to RAM
  // once. Copy them to a temporary buffer here, LMIC_setSession will
  // copy them into a buffer of its own again.
  uint8_t appskey[sizeof(APPSKEY)];
  uint8_t nwkskey[sizeof(NWKSKEY)];
  memcpy_P(appskey, APPSKEY, sizeof(APPSKEY));
  memcpy_P(nwkskey, NWKSKEY, sizeof(NWKSKEY));
  LMIC_setSession (0x13, DEVADDR, nwkskey, appskey);
#else
  // If not running an AVR with PROGMEM, just use the arrays directly
  LMIC_setSession (0x13, DEVADDR, NWKSKEY, APPSKEY);
#endif


#if defined(CFG_in866)
  // Set up the channels used in your country. Three are defined by default,
  // and they cannot be changed. Duty cycle doesn't matter, but is conventionally
  // BAND_MILLI.
  LMIC_setupChannel(0, 865062500, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_MILLI);
  LMIC_setupChannel(1, 865402500, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_MILLI);
  LMIC_setupChannel(2, 865985000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_MILLI);
  LMIC_setupChannel(3, 865232500, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
  LMIC_setupChannel(4, 866185000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
  LMIC_setupChannel(5, 866385000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
  LMIC_setupChannel(6, 866585000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
  LMIC_setupChannel(7, 866785000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band

  // ... extra definitions for channels 3..n here.
#else
# error Region not supported
#endif

  // Disable link check validation
  LMIC_setLinkCheckMode(0);

  LMIC.dn2Dr = DR_SF12;
  // Set data rate and transmit power for uplink
  LMIC_setDrTxpow(DR_SF12, 14);
  // Start job
  do_send(&sendjob);
}

void loop() {

#ifndef SLEEP

  os_runloop_once();

#else

  if (next == false) {

    os_runloop_once();

  } else {
  
// calculate the number of sleepcycles (8s) given the TX_INTERVAL
    int sleepcycles = (TX_INTERVAL / 8);
    Serial.println((String)"Enter sleeping for " + sleepcycles + "cycles of 8 seconds");
    delay(500);

    for (int i = 0; i < sleepcycles; i++) {
      // Enter power down state for 8 s with ADC and BOD module disabled
      LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);

    }
    Serial.println(F("Sleep complete"));
    next = false;

    do_send(&sendjob);
  }
#endif

}

/**
 *
 * @brief Read Sensor Data
 * @param none
 * @retval none
 *
 **/
 
void read_distance()
{
  // Activate the sensor by setting sensor_power_pin to HIGH and introduce a delay
  digitalWrite(sensor_power_pin, HIGH);
  delay(1000);
  // Initialize serial communication with a baud rate of 9600
  myserial.begin(9600);
  delay(100);
  // Record the starting time for error handling purposes
  previous_time = millis();
  while (1)
  {
    current_time = millis();
    if (previous_time > current_time)
    {
      previous_time = 0;
    }
    if (current_time - previous_time >= read_duration)
    {
      Serial.println("Serial Error");
      previous_time = current_time;
      mydata[0] = highByte(8000);
      mydata[1] = lowByte(8000);
      myserial.end();
      digitalWrite(sensor_power_pin, LOW);
      delay(1000);
      return;
    }
    else
    {
      if (myserial.available() > 0)
      {
        // Read data from the serial port
        do {
          for (uint8_t i = 0; i < 4; i++)
          {
            data[i] = myserial.read();
          }
        } while (myserial.read() == 0xff);

        myserial.flush();
      // Check if the data frame starts with 0xFF
        if (data[0] == 0xff)
        {
          int sum;
          // Calculate checksum and validate data
          sum = (data[0] + data[1] + data[2]) & 0x00FF;
          if (sum == data[3])
          {
           // Extract distance data and check if it is above a threshold
            distance = (data[1] << 8) + data[2];
            if (distance > 280)
            {
              Serial.println((String)"distance : " + distance + "mm");
              mydata[0] = highByte(distance);
              mydata[1] = lowByte(distance);
              previous_time = current_time;
              myserial.end();
              digitalWrite(sensor_power_pin, LOW);
              delay(1000);
              return;
            } else
            {
              Serial.println("Distance not in range");
              previous_time = current_time;
              mydata[0] = highByte(250);
              mydata[1] = lowByte(250);
              myserial.end();
              digitalWrite(sensor_power_pin, LOW);
              delay(1000);
              return;
            }
          }
          else Serial.println("Checksum ERROR");
        }
      }
      delay(150);
    }
  }
}

/**
 *
 * @brief Battery Voltage
 * @param none
 * @retval none
 *
 **/
void batteryVoltage()
{
  uint16_t batteryVoltage = 0;
  for (uint8_t i = 0; i < Sample_rate; i++)
  {
    batteryVoltage += ((analogRead(battery_pin)>>2) * 2.0 * 3.3 / 255) * 1000;
    delay(10);
  }
  batteryVoltage /= Sample_rate;
  mydata[2] = highByte(batteryVoltage);
  mydata[3] = lowByte(batteryVoltage);
  Serial.println((String)"Battery Voltage : " + batteryVoltage) ;
}




